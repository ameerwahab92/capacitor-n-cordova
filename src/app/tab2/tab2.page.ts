import { Component } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { Network } from '@ionic-native/network/ngx';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

import { DataService } from '../service/data.service';

const { LocalNotifications } = Plugins;
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  user = {
    name: 'Kasim',
    age: 24
  }
  constructor(
    private network: Network,
    public alertController: AlertController,
    private router: Router,
    public dataService: DataService
  ) { }

  ionViewWillEnter() {
    debugger;
    let networkType = this.network.type;
    this.presentAlert(networkType);
  }

  async presentAlert(messageNetwork) {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: messageNetwork,
      buttons: ['OK']
    });

    await alert.present();
  }

  clickNetworkInfo() {
    let connection = this.network.onConnect().subscribe(() => {
      console.log('network connected!');

      setTimeout(() => {
        if (this.network.type === 'wifi') {
          console.log('we got a wifi connection, woohoo!');
          // this.presentAlert();
        } else {
          console.log('Else condition');
        }
      }, 3000);
    });
  }
  clickLocalNotification() {
    // LocalNotifications.areEnabled();
    LocalNotifications.schedule({
      notifications: [
        {
          title: "Title",
          body: "Body",
          id: 1,
          // schedule: { at: new Date(Date.now() + 1000 * 5) },
          sound: null,
          attachments: null,
          actionTypeId: "",
          extra: null
        }
      ]
    }).catch(err => {
      console.error(err);
    });
    console.info("enter here");
  }


  goPageDetail() {
    //An example for page navigation
    // this.router.navigateByUrl('/details');

    //Example navigation with data
    this.dataService.data = this.user;
    this.router.navigateByUrl('/details/');

  }
}
