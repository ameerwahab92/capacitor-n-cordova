import { Component } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { Firebase } from '@ionic-native/firebase/ngx';

const { LocalNotifications } = Plugins;

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(private firebase: Firebase) { }

  clickLocalNotification() {
    // LocalNotifications.areEnabled();
    LocalNotifications.schedule({
      notifications: [
        {
          title: "Title",
          body: "Body",
          id: 1,
          // schedule: { at: new Date(Date.now() + 1000 * 5) },
          sound: null,
          attachments: null,
          actionTypeId: "",
          extra: null
        }
      ]
    }).catch(err => {
      console.error(err);
    });
    console.info("enter here");
  }
  fireBaseFunction() {
    this.firebase.getToken()
      .then(token => console.log(`The token is ${token}`)) // save the token server-side and use it to push notifications to this device
      .catch(error => console.error('Error getting token', error));

    this.firebase.onNotificationOpen()
      .subscribe(data => console.log(`User opened a notification ${data}`));

    this.firebase.onTokenRefresh()
      .subscribe((token: string) => console.log(`Got a new token ${token}`));
  }
}
